<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\adminlte\widgets;

/**
 * Class DOBPicker
 * @package mtech\adminlte\widgets
 */
class DOBPicker extends \dosamigos\datepicker\DatePicker
{
    public $clientOptions = ['startView' => '3', 'format' => 'yyyy-mm-dd', 'orientation' => 'bottom', 'autoclose' => true];
    public $options = ['autocomplete' => 'off'];
}

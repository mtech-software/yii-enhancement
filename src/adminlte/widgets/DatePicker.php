<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\adminlte\widgets;

/**
 * Class DatePicker
 * @package mtech\adminlte\widgets
 */
class DatePicker extends \dosamigos\datepicker\DatePicker
{
    public $clientOptions = ['format' => 'yyyy-mm-dd', 'orientation' => 'bottom', 'autoclose' => true];
	public $options = ['autocomplete' => 'off'];
}

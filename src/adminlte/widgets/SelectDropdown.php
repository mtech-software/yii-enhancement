<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\adminlte\widgets;


use kartik\select2\Select2;

/**
 * Class SelectDropdown
 * @package mtech\adminlte\widgets
 */
class SelectDropdown extends Select2
{
    /**
     * @var string
     */
    public $theme = self::THEME_DEFAULT;

    /**
     * @var array
     */
    public $pluginOptions = ['allowClear' => true];

    /**
     * @var array
     */
    public $options = ['placeholder' => 'Select One'];
}
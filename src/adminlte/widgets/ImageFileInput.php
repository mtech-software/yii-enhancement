<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\adminlte\widgets;

use kartik\file\FileInput;

/**
 * Class ImageFileInput
 * @package mtech\adminlte\widgets
 */
class ImageFileInput extends FileInput
{
    /**
     * @var array
     */
    public $options = ['accept' => 'image/*'];

    /**
     * @var array
     */
    public $pluginOptions = [
        'allowedPreviewTypes' => ['image', 'video'],
        'allowedFileExtensions' => ['jpg', 'jpeg', 'png', 'gif'],
        'fileActionSettings' => [
            'showZoom' => false,
            'indicatorNew' => '',
        ],
        'dropZoneEnabled' => false,
        'showUpload' => false,
        'showClose' => false,
    ];
}

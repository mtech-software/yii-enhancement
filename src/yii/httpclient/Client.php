<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\httpclient;

/**
 * Class Client
 * @package mtech\yii\httpclient
 */
class Client extends \yii\httpclient\Client
{

}

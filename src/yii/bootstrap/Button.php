<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\bootstrap;

/**
 * Class Button
 * @package mtech\yii\bootstrap
 */
class Button extends \yii\bootstrap\Button
{

}

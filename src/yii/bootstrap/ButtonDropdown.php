<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\bootstrap;

/**
 * Class ButtonDropdown
 * @package mtech\yii\bootstrap
 */
class ButtonDropdown extends \yii\bootstrap\ButtonDropdown
{
    /**
     * @var string
     */
    public $label = 'Action';

    /**
     * @var array
     */
    public $options = ['class' => 'btn-primary btn-sm'];
}

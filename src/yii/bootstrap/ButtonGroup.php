<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\bootstrap;

/**
 * Class ButtonGroup
 * @package mtech\yii\bootstrap
 */
class ButtonGroup extends \yii\bootstrap\ButtonGroup
{

}

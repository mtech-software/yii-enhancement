<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\bootstrap;

/**
 * Class ActiveForm
 * @package mtech\yii\bootstrap
 */
class ActiveForm extends \yii\bootstrap\ActiveForm
{
    /**
     * @var array the input field config
     */
    public $fieldConfig = [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-10',
            'error' => '',
            'hint' => '',
        ],
    ];
}

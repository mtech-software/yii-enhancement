<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\console;

/**
 * Class Application
 * @package mtech\yii\console
 */
class Application extends \yii\console\Application
{
    /**
     * Returns the configuration of the built-in commands.
     * @return array the configuration of the built-in commands.
     */
    public function coreCommands()
    {
        return [
            'cache' => 'yii\console\controllers\CacheController',
            'help' => 'yii\console\controllers\HelpController',
            'message' => 'yii\console\controllers\MessageController',
            'migrate' => 'yii\console\controllers\MigrateController',
            'serve' => 'yii\console\controllers\ServeController',
        ];
    }
}

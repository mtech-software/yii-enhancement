<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\base;

use yii\base\InvalidConfigException;
use Yii;

/**
 * Class Security
 * @property \mtech\helpers\TokenHelper token
 * @package mtech\yii\base
 */
class Security extends \yii\base\Security
{
    /**
     * @var \mtech\helpers\TokenHelper
     */
    private $_token;

    /**
     * initialize the web token helper
     *
     * @param string|array $config
     * @throws InvalidConfigException
     */
    public function setToken($config)
    {
        $this->_token = Yii::createObject($config);
    }

    /**
     * @return \mtech\helpers\TokenHelper
     */
    public function getToken()
    {
        return $this->_token;
    }
}

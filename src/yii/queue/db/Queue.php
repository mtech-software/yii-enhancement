<?php

/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */
namespace mtech\yii\queue\db;

/**
 * Class Queue
 * @package mtech\yii\queue\db
 */
class Queue extends \yii\queue\db\Queue
{
    /**
     * @var bool
     */
    public $enabled = true;
    /**
     * @var string table name
     */
    public $tableName = '{{%system_queue}}';
    /**
     * @var string command class name
     */
    public $commandClass = Command::class;

    /**
     * @inheritdoc
     */
    protected function pushMessage($message, $ttr, $delay, $priority)
    {
        if ($this->enabled === false) {
            return 0;
        }

        try {
            $code = md5($this->channel . $message);
            $this->db->createCommand()->insert($this->tableName, [
                'code' => $code,
                'channel' => $this->channel,
                'job' => $message,
                'pushed_at' => time(),
                'ttr' => $ttr,
                'delay' => $delay,
                'priority' => $priority ?: 1024,
            ])->execute();
            $tableSchema = $this->db->getTableSchema($this->tableName);
            $id = $this->db->getLastInsertID($tableSchema->sequenceName);

            return $id;
        } catch (\Exception $e) {
            //ignore, mostly code duplicate
            \Yii::error($e);
        }

        return 0;
    }
}

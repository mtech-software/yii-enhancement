<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\queue\cli;

/**
 * Class Command
 * @package mtech\yii\queue\cli
 */
class Command extends \yii\queue\cli\Command
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $valid = parent::beforeAction($action);

        if ($this->canVerbose($action->id) && $this->verbose) {
            $this->queue->attachBehavior('verbose', [
                'class' => VerboseBehavior::class,
                'command' => $this,
            ]);
        }

        return $valid;
    }

    /**
     * @inheritdoc
     */
    protected function isWorkerAction($actionID)
    {
        return in_array($actionID, ['run' ,'listen'], true);
    }
}

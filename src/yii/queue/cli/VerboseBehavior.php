<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\queue\cli;

use common\base\queue\jobs\Task;
use yii\queue\ExecEvent;

/**
 * Class Verbose
 * @package mtech\yii\queue\cli
 */
class VerboseBehavior extends \yii\queue\cli\VerboseBehavior
{
    /**
     * @param ExecEvent $event
     * @return string
     */
    protected function jobTitle(ExecEvent $event)
    {
        if ($event->job instanceof Task) {
            $message = $event->job->getVerboseMessage();
            $extra = "attempt: $event->attempt";
            if ($pid = $event->sender->getWorkerPid()) {
                $extra .= ", pid: $pid";
            }
            return " [$event->id] $message ($extra)";
        }

        return parent::jobTitle($event);
    }
}

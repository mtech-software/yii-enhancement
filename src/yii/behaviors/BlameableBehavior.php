<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\behaviors;

use Yii;

/**
 * Class BlameableBehavior
 * @package mtech\yii\behaviors
 */
class BlameableBehavior extends \yii\behaviors\BlameableBehavior
{
    /**
     * @inheritdoc
     * @param \yii\base\Event $event
     */
    protected function getValue($event)
    {
        if ('MTECH_CONSOLE_MODE') {
            // return -1; //console mode
			if ($this->value === null && Yii::$app->has('user')) {
				$userId = Yii::$app->get('user')->id;
				if ($userId === null) {
					return -1;
				}

				return $userId;
			}
        }

        if (Yii::$app->user->isGuest) {
            return 0;
        }
        
        return parent::getValue($event);
    }
}

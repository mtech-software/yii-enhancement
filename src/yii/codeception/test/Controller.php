<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\codeception\test;

/**
 * Class Controller
 * @package mtech\yii\codeception\test
 */
class Controller extends \yii\web\Controller
{
}
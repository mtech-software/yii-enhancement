<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\helpers;

/**
 * Class StringHelper
 * @package mtech\yii\helpers
 */
class StringHelper extends \yii\helpers\StringHelper
{
    /**
     * This function will return integer from a version string
     * @param string $version
     * @return int
     */
    public static function resolveVersion($version)
    {
        if (is_scalar($version)) {
            $digits = preg_replace('/[^0-9]/', '', $version);
            return (int) $digits;
        }

        return 0;
    }

    /**
     * return the first character of each words in string
     * @param string $value
     * @return string
     */
    public static function shortForm($value)
    {
        $s = strtolower($value);
        $s = ucwords($s);
        return preg_replace('/[a-z\s+]/', '', $s);
    }

    /**
     * @param int $length
     * @param bool $alpha
     * @param bool $digit
     * @param string $case
     * @return string
     */
    public static function randomString($length = 10, $alpha = true, $digit = true, $case = 'lower')
    {
        $digits = '0123456789';
        $lower = 'abcdefghijklmnopqrstuvwxyz';
        $upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        //make sure at least one is selected
        if (!$alpha && !$digit) {
            $alpha = true;
            $digit = true;
        }

        $characters = '';
        if ($alpha) {
            $case = strtolower($case);
            if (!in_array($case, ['lower', 'upper', 'mix'])) {
                $case = 'lower';
            }

            switch ($case) {
                case 'lower':
                    $characters .= $lower;
                    break;
                case 'upper':
                    $characters .= $upper;
                    break;
                case 'mix':
                    $characters .= $lower . $upper;
                    break;
                default:
                    $characters .= $lower;
                    break;
            }
        }

        $characters .= $digit ? $digits : '';

        //the output
        $output = '';

        $len = strlen($characters);
        for ($p = 0; $p < $length; $p++) {
            $output .= $characters[mt_rand(0, $len-1)];
        }

        return $output;
    }
}

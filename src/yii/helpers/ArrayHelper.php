<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\helpers;

/**
 * Class ArrayHelper
 * @package mtech\yii\helpers
 */
class ArrayHelper extends \yii\helpers\ArrayHelper
{

}

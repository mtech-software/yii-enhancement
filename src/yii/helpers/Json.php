<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\helpers;

/**
 * Class Json
 * @package mtech\yii\helpers
 */
class Json extends \yii\helpers\Json
{
    /**
     * Validate is the given string is JSON
     * @param string $content
     * @return bool
     */
    public static function validate($content)
    {
        return substr($content, 0, 1) === '{';
    }
}

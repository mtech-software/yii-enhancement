<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\web;

/**
 * Class DbSession
 * @package mtech\yii\web
 */
class DbSession extends \yii\web\DbSession
{
    public $sessionTable = '{{%http_session}}';
}

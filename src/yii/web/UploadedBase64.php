<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\web;

use yii\web\UploadedFile;

/**
 * Class UploadedBase64
 * @package mtech\yii\web
 */
class UploadedBase64 extends UploadedFile
{
    /**
     * @param string $file
     * @param bool $deleteTempFile
     * @return bool|void
     */
    public function saveAs($file, $deleteTempFile = true)
    {
        return copy($this->tempName, $file);
    }
}
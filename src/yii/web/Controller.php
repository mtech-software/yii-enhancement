<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\web;

/**
 * Class Controller
 * @package mtech\yii\web
 */
class Controller extends \yii\web\Controller
{

}

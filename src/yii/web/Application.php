<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\web;

/**
 * Class Application
 * @package mtech\yii\web
 * @property \mtech\yii\i18n\Formatter $formatter
 * @property \mtech\mail\Mailer $mailer
 * @property \mtech\yii\caching\FileCache $cache
 * @property \mtech\yii\base\Security $security
 * @property \mtech\yii\queue\core\Queue queue
 * @property \mtech\audit\Audit audit
 * @property \mtech\services\Aws $aws
 * @property \mtech\services\Gcp $gcp
 * @property \mtech\services\Platform $platform
 */
class Application extends \yii\web\Application
{

}

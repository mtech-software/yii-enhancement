<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\data;

/**
 * Class Pagination
 * @package mtech\yii\data
 */
class Pagination extends \yii\data\Pagination
{

}

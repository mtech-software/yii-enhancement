<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\data;

/**
 * Class ActiveDataProvider
 * @package mtech\yii\data
 */
class ActiveDataProvider extends \yii\data\ActiveDataProvider
{

}

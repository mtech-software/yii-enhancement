<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\yii\grid;

/**
 * Class DataColumn
 * @package mtech\yii\grid
 */
class DataColumn extends \yii\grid\DataColumn
{
    /**
     * @var array
     */
    public $filterInputOptions = ['class' => 'form-control', 'id' => null, 'prompt' => '-- All --'];
}

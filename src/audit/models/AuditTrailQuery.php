<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\audit\models;

use mtech\yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[AuditTrail]].
 *
 * @see AuditTrail
 */
class AuditTrailQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return AuditTrail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AuditTrail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

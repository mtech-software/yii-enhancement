<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\audit\models;

use mtech\yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[AuditEntry]].
 *
 * @see AuditEntry
 */
class AuditEntryQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return AuditEntry[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AuditEntry|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

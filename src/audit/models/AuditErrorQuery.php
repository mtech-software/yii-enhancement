<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */


namespace mtech\audit\models;

use mtech\yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[AuditError]].
 *
 * @see AuditError
 */
class AuditErrorQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return AuditError[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AuditError|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Software
 */

namespace mtech\assets;

use yii\web\AssetBundle;

/**
 * Class BootstrapAsset
 * @package mtech\assets
 */
class BootstrapAsset extends AssetBundle
{
    /**
     * To be published
     * @var string
     */
    public $sourcePath = '@mtech/assets/bootstrap';

    /**
     * @var array
     */
    public $css = [
        'css/bootstrap.min.css'
    ];

    /**
     * @var array
     */
    public $js = [
        'js/bootstrap.min.js'
    ];
}

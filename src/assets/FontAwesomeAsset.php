<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link http://www.mtech.my
 * @copyright Copyright (c) Mtech Software
 */
namespace mtech\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class FontAwesomeAsset extends AssetBundle
{
    /**
     * To be published
     * @var string
     */
    public $sourcePath = __DIR__ . '/fontawesome';

    /**
     * @var array
     */
    public $css = [
        'css/fontawesome-all.min.css'
    ];

    /**
     * @var array
     */
    public $js = [
//        'js/fontawesome-all.js'
    ];
}

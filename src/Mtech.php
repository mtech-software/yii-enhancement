<?php
/**
 * @author Jackey Chua <jackeychua@mtechmy.com>
 * @link https://www.mtechmy.com
 * @copyright Copyright (c) Mtech Holdings
 */

namespace mtech;

/**
 * Class Mtech
 * @package mtech
 */
class Mtech
{
    /**
     * @return string
     */
    public static function powered()
    {
        return 'Powered by <a href="https://www.mtechmy.com" target="_blank" rel="external">Mtech Software</a>';
    }
}